import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.tree import DecisionTreeClassifier
import matplotlib.pyplot as plt
import seaborn as sb


# 1. PROBLEM STATEMENT AND READ DATA
pd.set_option('display.max_columns', 12)
pd.set_option('display.width',None)
data = pd.read_csv('datasets/titanic.csv')

# 2. DATA ANALYSIS
#print(data.info())
# feature statistic
#print(data.describe())

# 3. DATA CLEANSING
# fill NaNs with mean value
data.Age = data.Age.fillna(data.Age.mean())
# fill NaNs with mode value
data.Embarked = data.Embarked.fillna(data.Embarked.mode()[0])
# delete NaN rows
#data.where(data.Cabin.notnull(), inplace = True)
#print(data.loc[data.Age.isnull()].head())

data_train = data.loc[:, ['Pclass','Sex','Age','SibSp','Parch','Fare','Embarked']] #DataFrame
labels = data['Survived']  # Series


le = LabelEncoder()
data_train.Sex = le.fit_transform(data_train['Sex'])

ohe = OneHotEncoder(dtype=int, sparse=False)
embarked = ohe.fit_transform(data_train.Embarked.to_numpy().reshape(-1,1))
data_train.drop(columns=['Embarked'], inplace=True)
data_train = data_train.join(pd.DataFrame(data=embarked, columns=ohe.get_feature_names(['Embarked'])))
#print(data_train.head())


#5 DATA TRAINING

dtc_model = DecisionTreeClassifier(criterion='entropy')
X_train, X_test, y_train, y_test = train_test_split(data_train,labels,train_size=0.7,random_state=123,shuffle=False)
dtc_model.fit(X_train, y_train)
labels_predicted = dtc_model.predict(X_test)
print(labels_predicted)
ser_pred = pd.Series(data=labels_predicted, name='Predicted',index=X_test.index)
res_df = pd.concat([X_test, y_test, ser_pred],axis=1)
print(res_df.head())
print('Model score:',dtc_model.score(X_test,y_test))


plt.subplots(1,1,figsize=(8,3),dpi=500)