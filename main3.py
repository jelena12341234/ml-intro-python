import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.tree import DecisionTreeClassifier
import matplotlib.pyplot as plt
import seaborn as sb
from sklearn import tree


# 1. PROBLEM STATEMENT AND READ DATA
pd.set_option('display.width',None)
#ucitavanje skupa podataka
data = pd.read_csv('datasets/cakes_train.csv')
#data.insert(loc=0, column='Id', value=np.arange(0,len(data),1))

#data modif


data.loc[:, ['eggs']] = data.loc[:, ['eggs']] * 63
print(data.head(10))

for index in data.index:
    sum = data.loc[index,'flour'] + data.loc[index,'eggs'] + data.loc[index,'sugar'] + data.loc[index,'milk'] + data.loc[index,'butter'] + data.loc[index,'baking_powder']
    data.loc[index,'flour'] = data.loc[index,'flour'] / sum * 100
    data.loc[index, 'eggs'] = data.loc[index, 'eggs'] / sum * 100
    data.loc[index, 'milk'] = data.loc[index, 'milk'] / sum * 100
    data.loc[index, 'butter'] = data.loc[index, 'butter'] / sum * 100
    data.loc[index, 'baking_powder'] = data.loc[index, 'baking_powder'] / sum * 100
    data.loc[index, 'sugar'] = data.loc[index, 'sugar'] / sum * 100



#ispis prvih i poslednjih 5
print(data.head(5))
print(data.tail(5))

#prikaz konciznih informacija i statistike
print(data.info())
print(data.describe())

#graficki prikaz izlaznog atributa od svakog ulaznog
X_flour = data.loc[:, ['flour']]
X_eggs = data.loc[:, ['eggs']]
X_sugar = data.loc[:, ['sugar']]
X_milk = data.loc[:, ['milk']]
X_butter = data.loc[:, ['butter']]
X_baking_powder = data.loc[:, ['baking_powder']]


X = X_eggs
Y = data['type']



#Labele za ose i naslov
plt.figure('Type of cake')
plt.xlabel('Input value', fontsize=13)
plt.ylabel('Type', fontsize=13)
plt.title('Cakes')


#ovde menjam ulazne parametre umesto X_area
plt.scatter(X, Y, s=23, c='red', marker='o', alpha=0.7, edgecolors='black', linewidths=2)
plt.show()

#graficki prikaz korelacione matrice svih atributa
#reference https://www.tutorialspoint.com/machine_learning_with_python/machine_learning_with_python_correlation_matrix_plot.htm

vars = ['flour','eggs','sugar','milk','butter','baking_powder']
correlations = data.corr()
fig = plt.figure()
ax = fig.add_subplot(111)
cax = ax.matshow(correlations, vmin=-1, vmax=1)
fig.colorbar(cax)
ticks = np.arange(0,6,1)
ax.set_xticks(ticks)
ax.set_yticks(ticks)
ax.set_xticklabels(vars)
ax.set_yticklabels(vars)
plt.show()


# 4. FEATURE ENGINEERING
#useful information == all
#ovde se vrsi odabir parametara za testiranje, izbaciti po zelji
data_train = data.loc[:,['flour','eggs','sugar','milk','butter','baking_powder']] #dataFrame
labels = data.loc[:,'type']
#======
le = LabelEncoder()
print(labels)
#labels = le.fit_transform(labels)
#print(labels)


#5 DATA TRAINING

dtc_model = DecisionTreeClassifier(criterion='entropy')
X_train, X_test, y_train, y_test = train_test_split(data_train,labels,train_size=0.8,random_state=256,shuffle=False)
dtc_model.fit(X_train, y_train)
labels_predicted = dtc_model.predict(X_test)
print(labels_predicted)
ser_pred = pd.Series(data=labels_predicted, name='Predicted',index=X_test.index)
res_df = pd.concat([X_test, y_test, ser_pred],axis=1)
print(res_df.head())
print('Model score:',dtc_model.score(X_test,y_test))


fig, axes =plt.subplots(1,1,figsize=(8,3),dpi=200)
tree.plot_tree(decision_tree=dtc_model, max_depth=3, feature_names=data_train.columns, class_names=['Cupcake','Muffin'], fontsize=3, filled=True)
plt.show()


#TREBA DODATI PRIKAZ FJE GRESKE