import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.tree import DecisionTreeClassifier
import matplotlib.pyplot as plt
import seaborn as sb
from LinearRegressionGradientDescent import LinearRegressionGradientDescent



# 1. PROBLEM STATEMENT AND READ DATA
pd.set_option('display.width',None)
#ucitavanje skupa podataka
data = pd.read_csv('datasets/house_prices_train.csv')
#data.insert(loc=0, column='Id', value=np.arange(0,len(data),1))

#ispis prvih i poslednjih 5
print(data.head(5))
print(data.tail(5))

#prikaz konciznih informacija i statistike
print(data.info())
print(data.describe())

#graficki prikaz izlaznog atributa od svakog ulaznog
X_area = data.loc[:, ['Area']]
X_yearBuilt = data.loc[:, ['Year_built']]
X_BathNo = data.loc[:, ['Bath_no']]
X_BedroomNo = data.loc[:,['Bedroom_no']]

X = X_area

Y = data['Price']
Y = Y / 1000
#Labele za ose i naslov
plt.figure('House prices')
plt.xlabel('Input value', fontsize=13)
plt.ylabel('Price in $', fontsize=13)
plt.title("House price")

#ovde menjam ulazne parametre umesto X_area
plt.scatter(X, Y, s=23, c='red', marker='o', alpha=0.7, edgecolors='black', linewidths=2)
plt.show()




#realizacija algoritma koristeci ugradjeni model
#ovde se vrsi odabir atributa
data_train = data.loc[:,['Year_built','Area','Bedroom_no']] #dataFrame
labels = data.loc[:,'Price']
lr_model = LinearRegression()
X_train, X_test, y_train, y_test = train_test_split(data_train,labels,train_size=0.1,random_state=150,shuffle=False)
lr_model.fit(X_train, y_train)


labels_predicted = lr_model.predict(X_test)
print(labels_predicted)
ser_pred = pd.Series(data=labels_predicted, name='Predicted',index=X_test.index)
res_df = pd.concat([X_test, y_test, ser_pred],axis=1)
print(res_df.head())
print('Model score:',lr_model.score(X_test,y_test))




