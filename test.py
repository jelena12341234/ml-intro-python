import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.tree import DecisionTreeClassifier
import matplotlib.pyplot as plt
import seaborn as sb
from sklearn import tree
#flour,eggs,sugar,milk,butter,baking_powder

# 1. PROBLEM STATEMENT AND READ DATA
pd.set_option('display.width',None)
#ucitavanje skupa podataka
data = pd.read_csv('datasets/cakes_train.csv')

data.loc[:, ['eggs']] = data.loc[:, ['eggs']] * 63
print(data.head(10))

for index in data.index:
    sum = data.loc[index,'flour'] + data.loc[index,'eggs'] + data.loc[index,'sugar'] + data.loc[index,'milk'] + data.loc[index,'butter'] + data.loc[index,'baking_powder']
    data.loc[index,'flour'] = data.loc[index,'flour'] / sum * 100
    data.loc[index, 'eggs'] = data.loc[index, 'eggs'] / sum * 100
    data.loc[index, 'milk'] = data.loc[index, 'milk'] / sum * 100
    data.loc[index, 'butter'] = data.loc[index, 'butter'] / sum * 100
    data.loc[index, 'baking_powder'] = data.loc[index, 'baking_powder'] / sum * 100
    data.loc[index, 'sugar'] = data.loc[index, 'sugar'] / sum * 100


print(data.head(10))


